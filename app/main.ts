require("@nativescript/firebase")
import Vue from 'nativescript-vue'
import App from './components/App.vue'



// Prints Vue logs when --env.production is *NOT* set while building

// @ts-ignore
Vue.config.silent = (TNS_ENV === 'production')

// @ts-ignore
new Vue({
  render: h => h('frame', [h(App)])
}).$start()
